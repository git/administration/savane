<?php
# Show last logins.
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_once ('../include/init.php');

# We don't internationalize messages in this file because they are
# for Savannah admins who use English.

site_admin_header (
  ['title' => no_i18n ("Check Last Logins"), 'context' => 'admhome']
);

$res_logins = db_execute ("
  SELECT s.user_id, s.ip_addr, s.time, u.user_name
  FROM session s, user u
  WHERE s.user_id = u.user_id AND s.user_id > 0 AND s.time > 0
  ORDER BY s.time DESC LIMIT 250"
);

if (db_numrows($res_logins) < 1)
  {
    $feedback = no_i18n("No records found, there must be an error somewhere.");
    $HTML->footer ([]);
    exit (0);
  }
print '<p>' . no_i18n ("Follow most recent logins:") . "</p>\n";
$title_arr = [no_i18n ("User Name"), no_i18n ("Ip"), no_i18n ("Date")];
print html_build_list_table_top ($title_arr);

$inc = 0;
while ($row_logins = db_fetch_array ($res_logins))
  {
    print '<tr class="' . utils_altrow ($inc++) . '">';
    print "<td>$row_logins[user_name]</td>\n<td>$row_logins[ip_addr]</td>\n";
    print "<td>" . utils_format_date($row_logins['time']) . "</td>\n";
    print "</tr>\n";
  }
print "</table>\n";
$HTML->footer ([]);
?>
