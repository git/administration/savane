<?php
# Form functions.
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

$dir_name = dirname (__FILE__);
foreach (['spam', 'random-bytes', 'form-check'] as $i)
  require_once ("$dir_name/$i.php");

function form_get_id ()
{
  static $form_id = null;
  if (!empty ($form_id))
    return $form_id;
  $uid = user_getid ();
  $form_id = random_hash ($uid);
  $result = db_autoexecute ('form',
    ['form_id' => $form_id, 'timestamp' => time (), 'user_id' => $uid],
    DB_AUTOQUERY_INSERT
  );
  if (db_affected_rows ($result) != 1)
    fb (_("System error while creating the form, report it to admins"), 1);
  return $form_id;
}

function form_id_input ($method)
{
  if ($method != 'post')
    return '';
  return form_hidden (['form_id' => form_get_id ()]);
}

# To use this form that disallow duplicates:
#    - form_header must be used on the form
#    - form_check must be used before any insert in the DB after submission

# Start the form with unique ID, store it in the database.
function form_header ($action = null, $method = "post", $extra = false)
{
  if ($action === null)
    $action = $GLOBALS["php_self"];
  if ($extra)
    $extra = " $extra";
  return "\n<form action=\"$action\" method=\"$method\"$extra>\n"
    . form_id_input ($method);
}

# Similar to form_header, but with a different argument parser.
function form_tag ($args = [], $action_suffix = '')
{
  $def_args = ['action' => $GLOBALS['php_self'], 'method' => 'post'];

  foreach ($def_args as $k => $v)
    if (empty ($args[$k]))
      $args[$k] = $def_args[$k];

  $args['action'] .= $action_suffix;
  $attr = '';
  foreach ($args as $k => $v)
    $attr .= " $k=\"$v\"";

  return "<form $attr>\n" . form_id_input ($args['method']);
}

# Usual input.
function form_input ($type, $name, $value = "", $extra = false)
{
  if ($value !== "")
    $value = 'value="' . utils_specialchars ($value) . '"';
  if ($extra)
    $extra = " $extra";
  $id_attr = " id=\"$name\"";
  if ($type == 'hidden' || $type == 'submit' || $type == 'radio')
    $id_attr = '';
  return "<input type=\"$type\"$id_attr name=\"$name\" $value$extra />";
}

function form_set_label_attr (&$attr)
{
  if (!array_key_exists ('label', $attr))
    return null;
  $ret = $attr['label'];
  unset ($attr['label']);
  return $ret;
}

function form_radio ($name, $value, $attr)
{
  $label = form_set_label_attr ($attr);
  $extra = '';
  if (!empty ($attr['checked']))
    $extra .= "checked='checked' ";
  if (empty ($attr['id']) && $label)
    $attr['id'] = "val_{$value}_$name";
  if (!empty ($attr['id']))
    $extra .= "id=\"{$attr['id']}\"";
  $ret = form_input ('radio', $name, $value, $extra);
  if (null === $label || empty ($attr['id']))
    return $ret;
  return $ret . html_label ($attr['id'], $label);
}

function form_checkbox ($name, $is_checked = 0, $attr = [])
{
  $label = form_set_label_attr ($attr);
  $extra = '';
  if ($is_checked)
    $extra .= ' checked="checked"';
  if (!empty ($attr))
    foreach ($attr as $k => $v)
      $extra .= " $k=\"$v\"";
  $val = '1';
  if (isset ($attr['value']))
    $val = '';
  $ret = form_input ('checkbox', $name, $val, $extra);
  if (null === $label)
    return $ret;
  return $ret . html_label ($name, $label);
}

function form_option ($value, $selected_value = NULL, $label = NULL)
{
  if ($label === NULL)
    $label = $value;
  $ret = "<option value=\"$value\"";
  if ($selected_value !== NULL)
     {
       if (!is_array ($selected_value))
         $selected_value = [$selected_value];
       if (in_array ($value, $selected_value))
         $ret .= ' selected="selected"';
     }
  return "$ret>$label</option>\n";
}

function form_hidden ($name_val)
{
  $ret = '';
  foreach ($name_val as $name => $val)
    $ret .= "<input type='hidden' name=\"$name\" value=\"$val\" />\n";
  return $ret;
}

# Special input: textarea.
function form_textarea ($name, $value="", $extra=false)
{
  if ($extra)
    $extra = " $extra";

  return "\n<textarea id=\"$name\" name=\"$name\"$extra>$value</textarea>";
}

# Add submit button.
function form_submit ($text = false, $submit_name = "update", $extra = false)
{
  global $int_trapisset;
  if (!$text)
    $text = _("Submit");

  # Add a trap for spammers: a text input that will have to be kept empty.
  # This won't prevent tailored bots to spam, but that should prevent
  # the rest of them, which is good enough (task #4151).
  # Sure, some bots will someday implement CSS support, but the ones that does
  # not will not disappear as soon as this happen.
  $trap = '';
  if (empty ($int_trapisset) && !user_isloggedin ())
    {
      $trap = " " . form_input ("text", "website", "http://");
      $int_trapisset = true;
    }
  return form_input ("submit", $submit_name, $text, $extra) . $trap;
}

function form_image ($img, $text, $name)
{
  $extra = html_image_attributes ($img, ['alt' => $text]);
  return form_input ('image', $name, '', $extra);
}

function form_image_trash ($name, $text = null)
{
  if ($text === null)
    $text = _('Delete');
  $extra = html_image_trash_attributes (['alt' => $text]);
  return form_input ('image', $name, '', $extra);
}
# Close the form, with submit button.
function form_footer ($text = false, $submit_name = "update")
{
  return "\n<div class='center'>\n" . form_submit ($text, $submit_name)
    . "</div>\n</form>\n";
}
?>
