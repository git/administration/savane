<?php
# Handling spam.
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# First, initialize some globals var: conffile allows two vals for
# admin conveniency.
if (!empty ($GLOBALS['sys_spamcheck_spamassassin']))
  {
    if ($GLOBALS['sys_spamcheck_spamassassin'] == 1)
      $GLOBALS['sys_spamcheck_spamassassin'] = "anonymous";
    elseif ($GLOBALS['sys_spamcheck_spamassassin'] == 2)
      $GLOBALS['sys_spamcheck_spamassassin'] = "all";
  }

$GLOBALS['int_probablyspam'] = false;
$GLOBALS['int_delayspamcheck_comment_id'] = false;

function spam_flag_notification ($item, $comment, $user, $reporter)
{
  global $sys_mail_domain, $sys_mail_admin, $sys_default_domain, $sys_home;

  # No i18n: the message is sent to Savannah admins.
  $message['subject'] = 'Spam reported';
  $spammer = 'anonymous';
  if ($user != 100)
    $spammer = "<" . user_getname ($user) . ">";

  $message['body'] = "User <" . user_getname ($reporter) . "> reported spam "
    . "posted by $spammer at\nhttps://$sys_default_domain$sys_home"
    . ARTIFACT . "/index.php?item_id=$item"
    . "&func=viewspam&comment_internal_id=$comment#spam$comment";
  sendmail_mail (
    ['to' =>  "$sys_mail_admin@$sys_mail_domain"],
    $message, ['tracker' => ARTIFACT, 'item' => $item]
  );
}

# Mark a spam.  This assumes that checks for user's permissions
# have been made already.
function spam_flag (
  $item_id, $comment_id, $score, $group_id, $reporter_user_id = 0
)
{
  if (!$reporter_user_id)
    $reporter_user_id = user_getid ();

  # Check if the reported haven't flagged the incriminated comment already.
  $result = db_execute ("
    SELECT id FROM trackers_spamscore
    WHERE
      item_id = ? AND artifact = ? AND comment_id = ?
      AND reporter_user_id = ?",
    [$item_id, ARTIFACT, $comment_id, $reporter_user_id]
  );
  if (db_numrows ($result))
    {
      fb (_("You already flagged this comment"), 1);
      return false;
    }

  # Find out who is the alleged spammer
  # (if comment_id = 0, then it is the item itself that is a spam).
  unset ($affected_user_id);
  if ($comment_id)
    # It is important to mention the field_name, to avoid malicious attempt
    # to mess with any other part of history.
    $result = db_execute ("
      SELECT mod_by AS user_id FROM " . ARTIFACT . "_history
      WHERE bug_history_id = ? AND field_name = 'details' AND bug_id = ?",
      [$comment_id, $item_id]
    );
  else
    $result = db_execute ("
      SELECT submitted_by AS user_id FROM " . ARTIFACT . "
      WHERE bug_id = ?", [$item_id]
    );
  $affected_user_id = db_result ($result, 0, 'user_id');

  # Affected user may be 100 (anonymous) or anything else but 0.
  # If it is zero, something went wrong, let assume the worse and stop here.
  if (!$affected_user_id)
    {
      fb (
        _("Not able to find out who submitted the alleged spam, "
          . "stopping here"),
        1
      );
      return false;
    }

  # If the affected user is member of the group that owns the item
  # assume that someone is trying to do something stupid. The code does not
  # allow to flag as spam items posted by group members.
  if ($affected_user_id != 100 && member_check ($affected_user_id, $group_id))
    exit_permission_denied ();

  # Feed the spamscore table.
  db_autoexecute ('trackers_spamscore',
    [ 'score' => $score, 'affected_user_id' => $affected_user_id,
      'reporter_user_id' => $reporter_user_id, 'artifact' => ARTIFACT,
      'item_id' => $item_id, 'comment_id' => $comment_id],
    DB_AUTOQUERY_INSERT
  );

  # Compute the score of the item.
  $newscore = spam_get_item_score ($item_id, ARTIFACT, $comment_id);

  # If newscore equal to score (so it was null in first place)
  # and the affected user is anonymous, increment of 3, that is the default
  # for anonymous post. We fill the database only for real users with positive
  # scores.
  if ($affected_user_id == 100 && $newscore == $score)
    $newscore += 3;

  # Update the item spamscore fields.
  if ($comment_id)
    db_execute ("
      UPDATE " . ARTIFACT . "_history SET spamscore = ?
      WHERE bug_history_id = ? AND field_name = 'details' AND bug_id = ?",
      [$newscore, $comment_id, $item_id]
    );
  else
    {
      $result = db_execute (
        "SELECT summary FROM " . ARTIFACT . " WHERE bug_id = ?", [$item_id]
      );
      # Get the current summary.
      $summary = db_result ($result, 0, 'summary');
      $arg = [];
      if ($newscore > 4)
        {
          if (strpos ($summary, '[SPAM]') === FALSE)
            $summary = "[SPAM] $summary";
          $arg['discussion_lock'] = 1;
        }

      $arg['spamscore'] = $newscore; $arg['summary'] = $summary;
      db_autoexecute (
        ARTIFACT, $arg, DB_AUTOQUERY_UPDATE, 'bug_id = ?', [$item_id]
      );
    }

  fb (sprintf (_("Flagged (+%s, total spamscore: %s)"), $score, $newscore));
  spam_flag_notification (
    $item_id, $comment_id, $affected_user_id, $reporter_user_id
  );

  # If the total spamscore is superior to 4, the content is supposedly
  # confirmed spam, then increment the user spamscore.
  if ($newscore < 5)
    return true;

  # If the affected_user_id is anonymous, end here, obviously we cannot
  # change any personal spamscore.
  if ($affected_user_id == 100)
    return true;

  # If the reporter already flagged a message of this user, end here
  # (we do not want a single user being able to increment by more than one
  # another user spamscore).
  if (spam_get_user_score ($affected_user_id, $reporter_user_id) > 1)
    return true;

  # Compute the score of the user.
  $userscore = spam_get_user_score ($affected_user_id);

  # Update the user spamscore field.
  db_execute (
    "UPDATE user SET spamscore = ? WHERE user_id = ?",
    [$userscore, $affected_user_id]
  );

  # No feedback about this last part, one user spamscore is the kind of info
  # that belongs to site admins territory.
  return true;
}

# Mark that a spam is actually not one
# (allow to set the tracker, because this function may be called from
# siteadmin area).
function spam_unflag ($item_id, $comment_id, $tracker, $group_id)
{
  # Update the spamscore table.
  db_execute ("
    DELETE FROM trackers_spamscore
    WHERE item_id = ? AND comment_id = ? AND artifact = ?",
    [$item_id, $comment_id, $tracker]
  );

  if (!ctype_alnum (strval ($tracker)))
    util_die (sprintf (_('Tracker &ldquo;%s&rdquo; is not valid (not alnum).'),
      $tracker)
    );

  # Update the item spamscore fields.
  if ($comment_id)
    db_execute ("
      UPDATE {$tracker}_history SET spamscore = 0
      WHERE bug_history_id = ? AND field_name = 'details' AND bug_id = ?",
      [$comment_id, $item_id]
    );
  else
    db_execute ("
      UPDATE $tracker SET spamscore = 0 WHERE bug_id = ? AND group_id = ?",
      [$item_id, $group_id]
    );
}

# Return the total score of a user.
function spam_get_user_score ($user_id = 0, $set_by_user_id = 0)
{
  if (!$user_id)
    $user_id = user_getid ();

  # Anonymous get always a score of 3 (requires two users to successfully
  # mark as spam something, only one group member).
  if ($user_id == 100)
    return 3;

  $set_by_user_id_sql = '';
  $set_by_user_id_params = [];
  if ($set_by_user_id)
    {
      $set_by_user_id_sql = "AND reporter_user_id = ?";
      $set_by_user_id_params = [$set_by_user_id];
    }

  # We cannot do a count because it does not allow us to use GROUP BY.
  $userscore = 0;
  $result = db_execute ("
    SELECT max(score) AS score FROM trackers_spamscore
    WHERE affected_user_id = ? $set_by_user_id_sql GROUP BY reporter_user_id",
    array_merge ([$user_id], $set_by_user_id_params)
  );
  while ($entry = db_fetch_array ($result))
    $userscore++;
  return $userscore;
}

# Return the total score of an item.
function spam_get_item_score ($item_id, $tracker, $comment_id)
{
  $result = db_execute ("
    SELECT score FROM trackers_spamscore
    WHERE item_id = ? AND artifact = ? AND comment_id = ?",
    [$item_id, $tracker, $comment_id]
  );
  $newscore = 0;
  while ($entry = db_fetch_array ($result))
    $newscore += $entry['score'];
  return $newscore;
}

# To be used when a comment or an item is created. It is not enough to
# update the spamscore field of $tracker and $tracker_history tables.
function spam_set_item_default_score (
  $item_id, $comment_id, $tracker, $score, $user_id
)
{
  # Nothing to do for anonymous post, spam_flag will properly interpret
  # the fact that the default is not specifically set.
  if ($user_id == 100)
    return;

  # If the score is null, there is obviously nothing to do.
  if ($score < 1)
    return;

  # If the score means spam, fill the global that will be used later
  # to skip mail notif.
  if ($score > 4)
    $GLOBALS['int_probablyspam'] = true;

  # Otherwise, add a new entry in the database, without mentioning the
  # affected user: we want to set the default score for the item, not to
  # increment the user spamscore.
  # We mark the user as reporter, so it is clear where do come from the flag.
  db_autoexecute ('trackers_spamscore',
    [ 'score' => $score, 'reporter_user_id' => $user_id,
      'artifact' => $tracker, 'item_id' => $item_id,
      'comment_id' => $comment_id], DB_AUTOQUERY_INSERT
  );
  fb (sprintf (_("Spam score of your post is set to %s"), $score), 1);
}

# Put an item or a comment in temporary queue.
function spam_add_to_spamcheck_queue (
  $item_id, $comment_id, $tracker, $group_id, $current_score
)
{
  # Useless if already considered as spam.
  if ($GLOBALS['int_probablyspam'])
    return false;

  # Check in config if we want to do such checks.
  if (!$GLOBALS['sys_spamcheck_spamassassin'])
    return false;

  # If user is member of the current group, stop anyway.
  if (member_check (0, $group_id))
    return false;

  # If logged in and we have to check only anonymous users, stop here.
  if ($GLOBALS['sys_spamcheck_spamassassin'] == "anonymous"
      && user_isloggedin ())
    return false;

  # Otherwise, add to the queue and arbitrarily change spamscore.
  $date = time ();
  $priority = 2;
  $newscore = $current_score + 5;

  # If anonymous, increment priority (yes, it will be meaningless on sites
  # where only anonymous posts are checked):
  # While we may consider giving the priority to logged in users for their
  # comfort, we have to take into account that we need to start with post
  # that are the most likely to contain spams.
  if (!user_isloggedin ())
    $priority++;

  db_execute ("
    INSERT INTO trackers_spamcheck_queue
    (artifact, item_id, comment_id, priority, date) VALUES (?, ?, ?, ?, ?)",
    [$tracker, $item_id, $comment_id, $priority, $date]
  );

  # We change only the item spamscore field, not the spamscore table:
  # it means that if any user unflag the item, it will be as if
  # there was no score yet.
  # (no discussion lock, update will generate notif if sent by users that
  # can skip this spam queue check - members, etc).
  if ($comment_id)
    $result = db_execute ("
      UPDATE {$tracker}_history SET spamscore = ?
      WHERE bug_history_id = ? AND field_name = 'details' AND bug_id = ?",
      [$newscore, $comment_id, $item_id]
    );
  else
    $result = db_execute ("
      UPDATE $tracker SET spamscore = ? WHERE bug_id = ? AND group_id = ?",
      [$newscore, $item_id, $group_id]
    );

  if (db_affected_rows ($result) > 0)
    {
      $msg = sprintf (
        _("Spam score of your post is set temporarily to %s, until it is "
          . "checked by spam\nfilters"), $newscore);
      fb ($msg, 1);
    }

  # The notification should be delayed.
  $GLOBALS['int_delayspamcheck_comment_id'] = $comment_id;
  $GLOBALS['int_delayspamcheck'] = true;

  return true;
}
