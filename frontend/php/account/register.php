<?php
# Register an account, part 1 (part 2 is e-mail confirmation)
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

$inc = ['init', 'sane', 'account', 'spam', 'form', 'utils', 'html', 'sendmail'];
foreach ($inc as $i)
  require_once ("../include/$i.php");

extract (sane_import ('post',
  [
    'name' => 'form_loginname',
    'pass' => ['form_pw', 'form_pw2', 'form_realname', 'form_email'],
    'digits' => 'form_year',
    'true' => ['update']
  ]
));
foreach (['form_realname', 'form_email', 'form_year'] as $v)
  if (!isset ($$v))
    $$v = '';

$form_email = preg_replace ('/\s/', '', $form_email);

if (isset ($sys_https_host) && !session_issecure ())
  # Force use of TLS for login.
  header ("Location: $sys_https_url{$_SERVER['REQUEST_URI']}");

# Logged users have no business here.
if (user_isloggedin ())
  session_redirect ($sys_home . 'my/');

$login_is_valid = false;
$pw_is_valid = false;
$email_is_valid = false;
$realname_is_valid = false;
$antispam_is_valid = false;

if (!empty ($update))
  {
    form_check ();
    # Form is submitted.
    if ($sys_registration_text_spam_test)
      {
        if ($form_year != 1983)
          fb (_("Please answer the antispam test!"), 1);
        else
          $antispam_is_valid = true;
      }
    if ($sys_registration_captcha)
      include_once '../captcha.php';
    if (!$sys_registration_captcha && !$sys_registration_text_spam_test)
      $antispam_is_valid = true;

    if ($form_loginname == '')
      fb (_("You must supply a username."), 1);
    elseif (!account_namevalid ($form_loginname))
      {
        # Feedback included by the check function.
      }
    # Avoid duplicates.
    elseif (
      db_numrows (db_execute (
        "SELECT user_id FROM user WHERE user_name = ?",
        [$form_loginname])) > 0
    )
      fb (_("That username already exists."), 1);
    elseif (
      db_numrows (db_execute (
       "SELECT group_list_id FROM mail_group_list WHERE list_name = ?",
       [$form_loginname])) > 0
    )
      fb (
        _("That username is blocked to avoid conflict with mailing-list "
          . "addresses."),
        1
      );
    else
      $login_is_valid = true;

    if ($form_pw == '')
      fb (_("You must supply a password."), 1);
    elseif ($form_pw != $form_pw2)
      fb (_("Passwords do not match."), 1);
    elseif (!account_pwvalid ($form_pw))
      {
        # Feedback included by the check function.
      }
    else
      $pw_is_valid = true;

    if (!$form_email)
      fb (_("You must supply a valid email address."), 1);
    elseif (!account_emailvalid ($form_email))
      {
        # Feedback included by the check function.
      }
    else
      $email_is_valid = true;

    $form_realname = account_sanitize_realname ($form_realname);
    if (account_realname_valid ($form_realname))
      $realname_is_valid = true;
  } # if (!empty ($update))
elseif ($sys_registration_captcha)
  {
    $antispam_is_valid = 'unset';
    include_once '../captcha.php';
    $antispam_is_valid = false;
  }

function check_duplicate_user_names ($user_id, $new_name)
{
  $result = db_execute (
    "SELECT user_id FROM user WHERE user_name = ?", [$new_name]
  );
  if (db_numrows ($result) < 2)
    return;
  user_purge ($user_id);
  exit_error (_("That username already exists."));
}

function create_user ()
{
  global $form_loginname, $form_pw, $form_realname, $form_email;
  $new_name = strtolower ($form_loginname);
  $passwd = account_encryptpw ($form_pw);
  $vals = ['user_name' => $new_name, 'user_pw' => $passwd,
    'status' => USER_STATUS_PENDING, 'realname' => $form_realname,
    'email' => $form_email, 'add_date' => time ()
  ];
  $result = db_autoexecute ('user', $vals);
  if (!$result)
    exit_error ('error', db_error ());
  $user_id = db_insertid ($result);
  check_duplicate_user_names ($user_id, $new_name);
  $hash = account_generate_confirm_hash (HASH_NEW_ACCOUNT, [], $user_id);
  if ($hash === null)
    exit_error ();
  return [$user_id, $hash];
}

function notify_user ($user_id, $confirm_hash)
{
  global $sys_name, $sys_https_url, $sys_home, $form_email;
  # TRANSLATORS: the argument is the name of the system (like "Savannah").
  $message_head = sprintf (
    _("Thank you for registering on the %s web site.\n"
      . "(Your login is not mentioned in this mail to prevent account "
      . "creation by robots.\n\n"
      . "In order to complete your registration, visit the following URL:"),
   $sys_name
  );
  $message_head .=  "\n$sys_https_url{$sys_home}"
    . "account/verify.php?confirm_hash=";
  $message_tail = "\n\n" . _("Enjoy the site.") . "\n\n"
    . utils_team_signature ();
  $message = "$message_head$confirm_hash$message_tail";
  sendmail_mail (
    ['to' => $form_email],
    # TRANSLATORS: the argument is the name of the system (like "Savannah").
    [ 'subject' => sprintf (_("%s account registration"), $sys_name),
      'body' => $message]
  );
  return "{$message_head}XXXXXXXXXXXXXXXXXXXXXXXXX$message_tail";
}

function congratulate ($user_id, $msg)
{
  global $sys_name, $sys_mail_replyto, $sys_mail_domain, $HTML;
  $HTML->header (['title' => _("Register Confirmation")]);
  print html_h (2, "$sys_name: " . _("New Account Registration Confirmation"));
  # TRANSLATORS: the argument is the name of the system (like "Savannah").
  printf (_("Congratulations. You have registered on %s."), $sys_name);
  print "\n";
  printf (_("Your login is %s."), '<b>' . user_getname ($user_id) . '</b>');
  print "\n<p>";
  printf (
    _("You are now being sent a confirmation email to verify your\nemail "
      . "address. Visiting the link sent to you in this email will activate "
      . "your\naccount. The email is sent from &lt;%s&gt;, it contains a "
      . "text like this:"),
    "$sys_mail_replyto@$sys_mail_domain"
  );
  print "</p>\n<blockquote><pre>\n$msg\n</pre></blockquote>\n<p><em>"
    . _("If you don't receive it within a reasonable time, contact website\n"
        . "administration.")
    . "</em></p>\n";
  $HTML->footer ([]);
}

$form_is_valid = $login_is_valid && $pw_is_valid && $email_is_valid
  && $realname_is_valid && $antispam_is_valid;

if ($form_is_valid)
  {
    list ($user_id, $confirm_hash) = create_user ();
    $msg = notify_user ($user_id, $confirm_hash);
    congratulate ($user_id, $msg);
    exit;
  } # if ($form_is_valid)

# Not valid registration, or first time at page.
site_header (
  ['title' => _("User account registration"), 'context' => 'account']
);
print form_header ();
$br = "</span><br />\n&nbsp;&nbsp;";
$pre = '<p><span class="preinput">';
print $pre . _("Login name:") . $br;
print form_input ("text", "form_loginname", $form_loginname);
print $pre . account_password_help () . '</span></p>';
print $pre . _("Passphrase:") . $br;
print form_input ("password", "form_pw", $form_pw) . "</p>\n";
print $pre . _("Re-type passphrase:") . $br;
print form_input ("password", "form_pw2", $form_pw2) . "</p>\n";
print $pre . _("Display name:") . $br;
print form_input ('text', 'form_realname', $form_realname, 'size="30"');
print "</p>\n";
print $pre . _("Email address:") . $br;
print form_input ('text', "form_email", $form_email, 'size="30"');
print "<br />\n" . '<span class="text">'
  . _("This email address will be verified before account activation.")
  . "</span></p>\n";

if ($sys_registration_text_spam_test)
  {
    print $pre . _("Antispam test:") . $br;
    print form_input ('text', "form_year", $form_year, 'size="30"');
    print "<br />\n<span class='text'>";
    printf (
      _("In what year was the GNU project announced? [<a href='%s'>hint</a>]"),
      'https://www.gnu.org/gnu/gnu-history.html'
    );
    print "</span></p>\n";
  }
if ($sys_registration_captcha)
  {
    $url = "{$sys_home}captcha.php";
    print "<img id='captcha' height='80' width='215' src=\"$url\" "
      . "alt='" . _('CAPTCHA') . "' /><br />\n";
    print "[ <a href='#' id='captcha_js_link'>" . _("Try another image")
      . "</a> ]\n"
      . "<script type='text/javascript' src='/js/captcha.php'></script>\n";
    print "[ <a href=\"$url?play=1\">" . _("Play captcha") . "</a> ]"
      . "<br />\n";
    print _("Antispam test:")
      . '<input type="text" name="captcha_code" size="17" '
      . 'maxlength="17" />';
  }
print form_footer ();
$HTML->footer ([]);
?>
