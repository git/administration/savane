<?php
# Request password recovery.
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_once ('../include/init.php');

# Logged users have no business here.
if (user_isloggedin ())
  session_redirect ("{$sys_home}my/");

$HTML->header (['title' => _("Lost Account Password")]);
print '<p><strong>' . _("Lost your password?") . "</strong></p>\n";

print '<p>'
  . _("The form below will email a URL to the email address we have on\nfile "
    . "for you. In this URL is a 128-bit confirmation hash for your account.\n"
    . "Visiting the URL will allow you to change your password online and\n"
    . "login.")
  . "</p>\n";
print '<p class="warn">'
  . _("This will work only if your account was already\nsuccessfully "
    . "registered and activated. Note that accounts that are not\nactivated "
    . "within the three days next to their registration are automatically\n"
    . "deleted.")
  . "</p>\n";

print form_header ('lostpw-confirm.php');
print '<p><span class="preinput"> &nbsp;&nbsp;';
print _("Login name:") . ' &nbsp;&nbsp;</span>';
print '<input type="text" name="form_loginname" /> &nbsp;&nbsp;';
print form_submit (_("Send lost password hash"), 'send') . "</p>\n";
print "</form>\n";

$HTML->footer ([]);
?>
