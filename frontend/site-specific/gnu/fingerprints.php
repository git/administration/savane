<?php
# vcs.savannah.gnu.org fingerprints; used in vcs-related pages.

# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev <ineiev@gnu.org>
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
$vcs_fingerprints = '
<pre>
1024 MD5:80:5a:b0:0c:ec:93:66:29:49:7e:04:2b:fd:ba:2c:d5 (RSA)
1024 SHA256:FYkx0iik+iBeCLRzvUyUSTRT98TEBBJoYuQsTXbyGL8 (RSA)

256 MD5:65:b8:1c:2f:82:7c:0e:39:e1:4a:63:f2:13:10:e8:9c (ECDSA)
256 SHA256:qRLLJ4w/GAeiDyYnbx4yWJbZXwGiYYxgNty7lAfUyuM (ECDSA)

256 MD5:14:7b:c8:98:dd:06:08:97:8c:00:9d:d2:ae:85:c8:82 (ED25519)
256 SHA256:o/oI4CKKcWc4cZvDFEdmOXsE3tiPP8bWa04h4bQjtV4 (ED25519)
</pre>

';
?>
