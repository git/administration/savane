#! @PERL@ -I@pkgdatadir@/lib
# -*-Perl-*-
# Assign uidNumber and gidNumber in the database.
#
# Copyright (C) 1999, 2000 The SourceForge Crew
# Copyright (C) 2000-2006 Mathieu Roy
# Copyright (C) 2014, 2016, 2017 Assaf Gordon
# Copyright (C) 2001-2011, 2013, 2017 Sylvain Beucler
# Copyright (C) 2013, 2014, 2017-2025 Ineiev
#
# This file is part of Savane.
#
# Code written before 2008-03-30 (commit 8b757b2565ff) is distributed
# under the terms of the GNU General Public license version 3 or (at your
# option) any later version; further contributions are covered by
# the GNU Affero General Public license version 3 or (at your option)
# any later version.  The license notices for the AGPL and the GPL follow.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use Savane;
use Getopt::Long;

# Using range 5000-oo for our groups.  1000-5000 should be reserved for
# /etc/passwd and /etc/group.

my $min_uidNumber = 5000;
my $min_gidNumber = 5000;

my $help =
"Usage: $0 [OPTIONS]

Assign uidNumber for users and gidNumber for groups.

Minimum IDs need to be set above your system maximum uidNumber and gidNumber,
so that the system can create users and groups in a range that is not used
by this script.

  -h, --help           show this help and exit
  -v, --version        show Savane version and exit
      --min-uidNumber  starting user ID [$min_uidNumber]
      --min-gidNumber  starting group ID [$min_gidNumber]";

PrintVersionOrHelp ($help);

# This is done in a cron job to avoid any race condition. Ideally
# uidNumber and gidNumber would be assign directly on account creation
# to avoid any delay.

# TODO: avoid assigning nobody(65534) and nogroup(65534).  Maybe we
# should just start at 65535 or 70000 (and update sv_users/sv_groups
# accordingly)...

# There are several sv_db2sys scripts but they should not run concurrently.
my $lockfile = 'assign_uid_gid';
AcquireReplicationLock ($lockfile);

# Fill remaining uidNumbers and gidNumbers.

GetOptions (
  "min-uidNumber=i" => \$min_uidNumber, "min-gidNumber=i" => \$min_gidNumber
) or die "Wrong command line";

sub DbdDo
{
  our $dbd;
  $dbd->do ($_[0]) or die "SQL Error: $DBI::errstr\n";
}

# Generate uidNumbers.
DbdDo (
  "CREATE TEMPORARY TABLE
   temp_uid_counter (user_id int, uidNumber int auto_increment PRIMARY KEY);"
);
DbdDo (
  "INSERT INTO temp_uid_counter (user_id, uidNumber)
   VALUES (0, $min_uidNumber-1);"
);

# Import existing uidNumbers.
DbdDo (
  "INSERT INTO temp_uid_counter (user_id, uidNumber)
   SELECT user_id, uidNumber FROM user WHERE uidNumber > 0;"
);

# Assign new uidNumbers.
DbdDo (
  "INSERT INTO temp_uid_counter (user_id)
   SELECT user_id FROM user WHERE uidNumber IS NULL AND status = 'A';"
);

# Update uidNumbers.
DbdDo (
  "UPDATE user, temp_uid_counter
   SET user.uidNumber = temp_uid_counter.uidNumber
   WHERE user.user_id = temp_uid_counter.user_id
     AND user.uidNumber IS NULL;"
);

# Generate gidNumbers.
DbdDo (
  "CREATE TEMPORARY TABLE
   temp_gid_counter (group_id int, gidNumber int auto_increment PRIMARY KEY);"
);
DbdDo (
  "INSERT INTO temp_gid_counter (group_id, gidNumber)
   VALUES (0, $min_gidNumber-1);"
);
# Import existing gidNumbers.
DbdDo (
  "INSERT INTO temp_gid_counter (group_id, gidNumber)
   SELECT group_id, gidNumber FROM groups WHERE gidNumber > 0;"
);
# Assign new gidNumbers.
DbdDo (
  "INSERT INTO temp_gid_counter (group_id)
   SELECT group_id FROM groups WHERE gidNumber IS NULL AND status = 'A';"
);

# Update gidNumbers.
DbdDo (
  "UPDATE groups, temp_gid_counter
   SET groups.gidNumber = temp_gid_counter.gidNumber
   WHERE groups.group_id = temp_gid_counter.group_id
     AND groups.gidNumber IS NULL;"
);

# Cache users/groups to make libmysql-nss-bg more efficient.

# This is a temporary work-around - a better is to fix
# libmysql-nss-bg's inefficient, 1 query / group getgrent
# implementation.  MySQL's GROUP_CONCAT will help.

DbdDo (
  "UPDATE user_group ug, user u, groups g
   SET
     ug.cache_uidNumber = u.uidNumber, ug.cache_gidNumber = g.gidNumber,
     ug.cache_user_name = u.user_name
   WHERE
     (
       ug.cache_uidNumber IS NULL OR ug.cache_gidNumber IS NULL
       OR ug.cache_user_name IS NULL
     )
     AND ug.user_id = u.user_id AND ug.group_id = g.group_id
     AND ug.admin_flags IN ('A', '');"
);
